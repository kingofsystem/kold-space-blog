import os
from pathlib import Path
import sys
from tempfile import mktemp

from PIL import Image


def convert_to_webp(file_path, destination, file_name):
    os.system(
        f'cwebp -preset photo {file_path} -o {destination}/{file_name}.webp'
    )


def resize_image(file_path):
    img = Image.open(file_path)
    img.thumbnail([1080, 1080], Image.ANTIALIAS)
    temp_filename = mktemp(suffix=f'.{file_path.name.split(".")[1]}')
    img.save(temp_filename)
    return Path(temp_filename)


def main():
    originals_path, compressed_path = (
        Path(sys.argv[1]).absolute(), Path(sys.argv[2]).absolute()
    )

    original_file_paths = tuple(originals_path.glob('*'))
    compressed_file_names = {
        file_path.name.split('.')[0]
        for file_path in compressed_path.glob('*')
    }

    for original_file_path in original_file_paths:
        if original_file_path.name.split('.')[0] in compressed_file_names:
            continue
        resized_image_path = resize_image(original_file_path)
        convert_to_webp(
            resized_image_path, compressed_path, original_file_path.name.split('.')[0]
        )
        resized_image_path.unlink()


if __name__ == '__main__':
    main()
