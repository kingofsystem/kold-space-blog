import os
from pathlib import Path


ORIGINAL_PHOTOS_PATH = Path(os.environ.get('ORIGINAL_PHOTOS_PATH', '/originals'))
COMPRESSED_PHOTOS_PATH = Path(os.environ.get('COMPRESSED_PHOTOS_PATH', '/compressed'))


def get_photos():
    original_path_by_name_map = {
        path.name.split('.')[0]: path
        for path in ORIGINAL_PHOTOS_PATH.glob('*')
    }

    photo_paths = sorted(
        COMPRESSED_PHOTOS_PATH.glob('*'),
        key=lambda item: item.stat().st_mtime,
        reverse=True,
    )

    return tuple(
        {
            'compressed_path': f'/compressed_images/{path.name}',
            'original_path': (
                f'/images/{original_path_by_name_map[path.name.split(".")[0]].name}'
            ),
        }
        for path in photo_paths
    )
