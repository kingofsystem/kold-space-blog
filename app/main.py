from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import HTMLResponse
from starlette.routing import Route
from starlette.templating import Jinja2Templates
from starlette_core.paginator import Paginator

import photos
import posts


templates = Jinja2Templates(directory='templates')


async def homepage(request: Request):
    photos_paginator = Paginator(photos.get_photos(), 10)

    page_number = request.query_params.get('page', 1)
    page = photos_paginator.page(page_number)

    return templates.TemplateResponse(
        name='photos.html',
        context={
            'page': page,
            'paginator': photos_paginator,
            'request': request,
        },
    )


async def posts_page(request: Request):
    posts_paginator = Paginator(posts.get_posts(), 11)

    page_number = request.query_params.get('page', 1)
    page = posts_paginator.page(page_number)

    return templates.TemplateResponse(
        name='posts.html',
        context={
            'request': request,
            'page': page,
            'paginator': posts_paginator,
        },
    )


async def post_page(request: Request):
    post_slug = request.path_params['post_slug']

    try:
        post = posts.get_post_by_slug(post_slug)
    except posts.PostNotFound:
        return HTMLResponse('Not Found!', 404)

    if not post.body:
        return HTMLResponse('Not Found!', 404)

    return templates.TemplateResponse(
        name='post.html',
        context={
            'request': request,
            'post': post,
        },
    )


app = Starlette(debug=False, routes=[
    Route('/photos/', homepage),
    Route('/', posts_page),
    Route('/post/{post_slug}/', post_page),
])
