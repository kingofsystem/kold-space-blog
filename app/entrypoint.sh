#!/bin/bash

gunicorn -w $(nproc --all) -k uvicorn.workers.UvicornWorker --access-logfile ./access_log.txt --log-level debug -b 172.34.0.3:8000 main:app
