import json
import os
from dataclasses import dataclass

from datetime import datetime
from pathlib import Path
from typing import List

import markdown


POSTS_PATH = Path(os.environ.get('POSTS_PATH', '/posts'))


@dataclass
class Post:
    slug: str
    created: datetime
    picture_url: str = None
    picture_path: Path = None
    header: str = None
    body: str = None
    short_text: str = None

    @property
    def has_body(self) -> bool:
        fields = list(self.__annotations__.keys())

        fields.remove('picture_url')
        fields.remove('picture_path')

        return any(
            getattr(self, field)
            for field in fields
        )


def build_post(path: Path) -> Post:

    meta_json_path = path / 'meta.json'
    if not meta_json_path.exists():
        raise ValueError(f'meta.json of the post must be present. Path: {path}')

    with meta_json_path.open() as meta_json_file:
        meta_json = json.load(meta_json_file)

    post = Post(
        slug=path.name,
        created=datetime.fromisoformat(meta_json['created']),
        header=meta_json['header'],
    )

    picture_path = path / 'picture.webp'
    if picture_path.exists():
        post.picture_path = picture_path
        post.picture_url = picture_path

    body_path = path / 'body.md'
    if body_path.exists():
        with body_path.open() as body_file:
            post.body = markdown.markdown(
                body_file.read(), extensions=('attr_list',)
            )

    short_text_path = path / 'short_text.txt'
    if short_text_path.exists():
        with short_text_path.open() as short_text_file:
            post.short_text = short_text_file.read()

    return post


class PostNotFound(Exception):
    pass


def get_post_by_slug(slug) -> Post:

    post_path = POSTS_PATH / slug
    if not post_path.exists() or not post_path.is_dir():
        raise PostNotFound(f'Post with slug {slug} does not exists')

    return build_post(
        post_path
    )


def get_posts() -> List[Post]:

    result = []
    for post_dir in POSTS_PATH.glob('*'):
        if not post_dir.is_dir():
            continue

        result.append(
            build_post(post_dir),
        )

    result.sort(key=lambda item: item.created, reverse=True)

    return result
